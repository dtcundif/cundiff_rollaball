﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rigidBody;
    private int count;
    private int pickedUp;
    private DateTime lastPoint;
    private DateTime lastPointCheck;
    private bool gameOver => pickedUp >= 12;
    private bool died;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        count = 0;
        pickedUp = 0;
        SetCountText();
        winText.text = "";
        died = false;

        lastPoint = DateTime.UtcNow;
    }

    private void Update()
    {
        if (transform.position.y <= -10)
        {
            pickedUp = 12;
            died = true;
            SetCountText();
            return;
        }

        if (!gameOver && (DateTime.UtcNow - lastPointCheck).TotalSeconds >= 5 && count > 0 && (DateTime.UtcNow - lastPoint).TotalSeconds >= 5)
        {
            lastPointCheck = DateTime.UtcNow;

            count--;
            SetCountText();
        }
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        float jump = Input.GetKeyDown(KeyCode.Space) ? 20f : 0;

        Vector3 force = new Vector3(moveHorizontal, jump, moveVertical);

        rigidBody.AddForce(force * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count++;
            pickedUp++;
            SetCountText();
        }
    }

    void SetCountText()
    {
        lastPoint = DateTime.UtcNow;
        countText.text = $"Count: {count}";

        if (gameOver)
        {
            if (count >= 12)
            {
                winText.text = "You Win!";
                return;
            }

            winText.text = died ? "You Died!" : "You Lose!";
        }
    }
}
